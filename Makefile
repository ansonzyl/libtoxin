LIBTOXIN := out/libtoxin_pin.a out/libtoxin_venom.a

LIBTOXIN_SRC := $(wildcard src/jni/*)
NATIVE_TEST_SRC := $(wildcard examples/1_native/jni/*)
DVM_TEST_SRC := $(wildcard examples/2_dvmBasic/jni/*)

.PHONY: all example1 example2 clean install uninstall

all: $(LIBTOXIN)

$(LIBTOXIN): $(LIBTOXIN_SRC)
	@echo "\n============= BUILDING LIBTOXIN ============"
	@cd src; ndk-build
	@mv src/obj/local/armeabi/lib*.a out/
	@rm -rf src/obj
	@echo "=============       DONE!       ============\n"

example1: $(LIBTOXIN) $(NATIVE_TEST_SRC)
	@echo "\n=========== BUILDING NATIVE_TEST ==========="
	@cd examples/1_native; ndk-build
	@mv examples/1_native/libs/armeabi/* out/
	@rm -rf examples/1_native/libs examples/1_native/obj
	@echo "===========         DONE!       ============\n"

example2: $(LIBTOXIN) $(DVM_TEST_SRC)
	@echo "\n=========== BUILDING DVM_TEST ==========="
	@cd examples/2_dvmBasic; ndk-build
	@mv examples/2_dvmBasic/libs/armeabi/* out/
	@rm -rf examples/2_dvmBasic/libs examples/2_dvmBasic/obj
	@echo "===========         DONE!       ============\n"

clean:
	@rm -rf out/*

# The following needs adb enabled
install:
	adb push out/test /sdcard/
	adb push out/libtoxin_inj.so /sdcard/
	adb shell su -c dd if=/sdcard/test of=/data/test
	adb shell su -c dd if=/sdcard/libtoxin_inj.so of=/data/libtoxin_inj.so
	adb shell su -c chmod 777 /data/test
	adb shell su -c chmod 777 /data/libtoxin_inj.so

uninstall:
	adb shell su -c rm /data/test /data/libtoxin_inj.so \
	   /sdcard/test /sdcard/libtoxin_inj.so
