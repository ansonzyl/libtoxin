#ifndef LIBTOXIN_VENOM_
#define LIBTOXIN_VENOM_

#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <stdint.h>
#include <android/log.h>

#define  LOGD(fmt, args...)  __android_log_print(ANDROID_LOG_DEBUG,"TOXIN_DEBUG", fmt, ##args)    
#define  LOGE(fmt, args...)  __android_log_print(ANDROID_LOG_ERROR,"TOXIN_ERROR", fmt, ##args)    
#define  LOGI(fmt, args...)  __android_log_print(ANDROID_LOG_INFO,"TOXIN_INFO", fmt, ##args)    

typedef enum { false=0, true=!false } bool;

typedef uint8_t             u1;
typedef uint16_t            u2;
typedef uint32_t            u4;
typedef uint64_t            u8;
typedef int8_t              s1;
typedef int16_t             s2;
typedef int32_t             s4;
typedef int64_t             s8;

typedef struct DexProto {
    u4* dexFile;     /* file the idx refers to */
    u4 protoIdx;     /* index into proto_ids table of dexFile */
} DexProto;

struct Field {
    void*    clazz;             /* class in which the field is declared */
    const char*     name;
    const char*     signature;  /* e.g. "I", "[C", "Landroid/os/Debug;" */
    u4              accessFlags;
};

struct Method;
struct ClassObject;

struct Object {
    struct ClassObject*    clazz;
    u4              lock;
};

struct InitiatingLoaderList {
    void**  initiatingLoaders;
    int     initiatingLoaderCount;
};

enum PrimitiveType {
    PRIM_NOT        = 0, /* value is a reference type, not a primitive type */
    PRIM_VOID       = 1,
    PRIM_BOOLEAN    = 2,
    PRIM_BYTE       = 3,
    PRIM_SHORT      = 4,
    PRIM_CHAR       = 5,
    PRIM_INT        = 6,
    PRIM_LONG       = 7,
    PRIM_FLOAT      = 8,
    PRIM_DOUBLE     = 9,
} typedef PrimitiveType;

enum ClassStatus {
    CLASS_ERROR         = -1,

    CLASS_NOTREADY      = 0,
    CLASS_IDX           = 1,    /* loaded, DEX idx in super or ifaces */
    CLASS_LOADED        = 2,    /* DEX idx values resolved */
    CLASS_RESOLVED      = 3,    /* part of linking */
    CLASS_VERIFYING     = 4,    /* in the process of being verified */
    CLASS_VERIFIED      = 5,    /* logically part of linking; done pre-init */
    CLASS_INITIALIZING  = 6,    /* class init in progress */
    CLASS_INITIALIZED   = 7,    /* ready to go */
} typedef ClassStatus;

struct ClassObject {
    struct Object o; 

    u4              instanceData[4];

    /* UTF-8 descriptor for the class; from constant pool, or on heap
       if generated ("[C") */
    const char*     descriptor;
    char*           descriptorAlloc;

    /* access flags; low 16 bits are defined by VM spec */
    u4              accessFlags;

    /* VM-unique class serial number, nonzero, set very early */
    u4              serialNumber;

    /* DexFile from which we came; needed to resolve constant pool entries */
    /* (will be NULL for VM-generated, e.g. arrays and primitive classes) */
    void*         pDvmDex;

    /* state of class initialization */
    ClassStatus     status;

    /* if class verify fails, we must return same error on subsequent tries */
    struct ClassObject*    verifyErrorClass;

    /* threadId, used to check for recursive <clinit> invocation */
    u4              initThreadId;

    /*
     * Total object size; used when allocating storage on gc heap.  (For
     * interfaces and abstract classes this will be zero.)
     */
    size_t          objectSize;

    /* arrays only: class object for base element, for instanceof/checkcast
       (for String[][][], this will be String) */
    struct ClassObject*    elementClass;

    /* arrays only: number of dimensions, e.g. int[][] is 2 */
    int             arrayDim;
    PrimitiveType   primitiveType;

    /* superclass, or NULL if this is java.lang.Object */
    struct ClassObject*    super;

    /* defining class loader, or NULL for the "bootstrap" system loader */
    struct Object*         classLoader;

    struct InitiatingLoaderList initiatingLoaderList;

    /* array of interfaces this class implements directly */
    int             interfaceCount;
    struct ClassObject**   interfaces;

    /* static, private, and <init> methods */
    int             directMethodCount;
    struct Method*         directMethods;

    /* virtual methods defined in this class; invoked through vtable */
    int             virtualMethodCount;
    struct Method*         virtualMethods;

    /*
     * Virtual method table (vtable), for use by "invoke-virtual".  The
     * vtable from the superclass is copied in, and virtual methods from
     * our class either replace those from the super or are appended.
     */
    int             vtableCount;
    struct Method**        vtable;

};

typedef void (*DalvikBridgeFunc)(const u4* args, void* pResult,
        const void* method, void* self);

typedef struct Method {
    struct ClassObject *clazz;
    u4  a; // accessflags
    u2  methodIndex;
    u2  registersSize;  /* ins + locals */
    u2  outsSize;
    u2  insSize;

    /* method name, e.g. "<init>" or "eatLunch" */
    const char*     name;

    /*
     * Method prototype descriptor string (return and argument types).
     *
     * TODO: This currently must specify the DexFile as well as the proto_ids
     * index, because generated Proxy classes don't have a DexFile.  We can
     * remove the DexFile* and reduce the size of this struct if we generate
     * a DEX for proxies.
     */
    DexProto        prototype;

    /* short-form method descriptor string */
    const char*     shorty;

    /*
     * The remaining items are not used for abstract or native methods.
     * (JNI is currently hijacking "insns" as a function pointer, set
     * after the first call.  For internal-native this stays null.)
     */

    /* the actual code */
    const u2*       insns;      

    /* cached JNI argument and return-type hints */
    int             jniArgInfo;

    /*
     * Native method ptr; could be actual function or a JNI bridge.  We
     * don't currently discriminate between DalvikBridgeFunc and
     * DalvikNativeFunc; the former takes an argument superset (i.e. two
     * extra args) which will be ignored.  If necessary we can use
     * insns==NULL to detect JNI bridge vs. internal native.
     */
    DalvikBridgeFunc  nativeFunc;

    bool fastJni;

    /*
     * JNI: true if this method has no reference arguments. This lets the JNI
     * bridge avoid scanning the shorty for direct pointers that need to be
     * converted to local references.
     *
     * TODO: replace this with a list of indexes of the reference arguments.
     */
    bool noRef;


} Method;

typedef void (*DalvikNativeFunc)(const u4* args, jvalue* pResult);

typedef struct DalvikNativeMethod_t {
    const char* name;
    const char* signature;
    DalvikNativeFunc  fnPtr;
} DalvikNativeMethod;

typedef void* (*dvmCreateStringFromCstr_func)(const char* utf8Str, int len, int allocFlags);
typedef void* (*dvmGetSystemClassLoader_func)(void);
typedef void* (*dvmThreadSelf_func)(void);

typedef void* (*dvmIsClassInitialized_func)(void*);
typedef void* (*dvmInitClass_func)(void*);
typedef void* (*dvmFindVirtualMethodHierByDescriptor_func)(void*,const char*, const char*);
typedef void* (*dvmFindDirectMethodByDescriptor_func)(void*,const char*, const char*);
typedef void* (*dvmIsStaticMethod_func)(void*);
typedef void* (*dvmAllocObject_func)(void*, unsigned int);
typedef void* (*dvmCallMethodV_func)(void*,void*,void*,void*,va_list);
typedef void* (*dvmCallMethodA_func)(void*,void*,void*,bool,void*,jvalue*);
typedef void* (*dvmAddToReferenceTable_func)(void*,void*);
typedef void (*dvmDumpAllClasses_func)(int);
typedef void* (*dvmFindLoadedClass_func)(const char*);

typedef void (*dvmUseJNIBridge_func)(void*, void*);

typedef void* (*dvmDecodeIndirectRef_func)(void*,void*);

typedef void* (*dvmGetCurrentJNIMethod_func)();

typedef void (*dvmLinearSetReadWrite_func)(void*,void*);

typedef void* (*dvmFindInstanceField_func)(void*,const char*,const char*);

typedef void* (*dvmSetNativeFunc_func)(void*,void*, void*);
typedef void (*dvmCallJNIMethod_func)(const u4*, void*, void*, void*);

typedef void (*dvmHashTableLock_func)(void*);
typedef void (*dvmHashTableUnlock_func)(void*);
typedef void (*dvmHashForeach_func)(void*,void*,void*);

typedef void (*dvmDumpClass_func)(void*,void*);

typedef int (*dvmInstanceof_func)(void*,void*);

typedef void (*JNI_GetCreatedJavaVMs_func)(JavaVM**, jsize, jsize*);


struct toxin_venom_t
{	
    void *dvm_hdl;  // Not the addr of libdvm.so, but the corresponding handle of linker
    void *gDvm; 

    JavaVM * jvm;
    JNIEnv * env;

    JNI_GetCreatedJavaVMs_func JNI_GetCreatedJavaVMs_fnPtr;
    dvmCreateStringFromCstr_func dvmStringFromCStr_fnPtr;
    dvmGetSystemClassLoader_func dvmGetSystemClassLoader_fnPtr;
    dvmThreadSelf_func dvmThreadSelf_fnPtr;

    dvmIsClassInitialized_func dvmIsClassInitialized_fnPtr;
    dvmInitClass_func dvmInitClass_fnPtr;
    dvmFindVirtualMethodHierByDescriptor_func dvmFindVirtualMethodHierByDescriptor_fnPtr;
    dvmFindDirectMethodByDescriptor_func dvmFindDirectMethodByDescriptor_fnPtr;
    dvmIsStaticMethod_func dvmIsStaticMethod_fnPtr;
    dvmAllocObject_func dvmAllocObject_fnPtr;
    dvmCallMethodV_func dvmCallMethodV_fnPtr;
    dvmCallMethodA_func dvmCallMethodA_fnPtr;
    dvmAddToReferenceTable_func dvmAddToReferenceTable_fnPtr;
    dvmDecodeIndirectRef_func dvmDecodeIndirectRef_fnPtr;
    dvmUseJNIBridge_func dvmUseJNIBridge_fnPtr;
    dvmFindInstanceField_func dvmFindInstanceField_fnPtr;
    dvmFindLoadedClass_func dvmFindLoadedClass_fnPtr;
    dvmDumpAllClasses_func dvmDumpAllClasses_fnPtr;

    dvmGetCurrentJNIMethod_func dvmGetCurrentJNIMethod_fnPtr;
    dvmLinearSetReadWrite_func dvmLinearSetReadWrite_fnPtr;

    dvmSetNativeFunc_func dvmSetNativeFunc_fnPtr;
    dvmCallJNIMethod_func dvmCallJNIMethod_fnPtr;

    dvmHashTableLock_func dvmHashTableLock_fnPtr;
    dvmHashTableUnlock_func dvmHashTableUnlock_fnPtr;
    dvmHashForeach_func dvmHashForeach_fnPtr;

    dvmDumpClass_func dvmDumpClass_fnPtr;
    dvmInstanceof_func dvmInstanceof_fnPtr;

    DalvikNativeMethod *dvm_dalvik_system_DexFile;
    DalvikNativeMethod *dvm_java_lang_Class;

    int done;
};

void toxin_resolv_dvm(struct toxin_venom_t *d);
int toxin_loaddex(struct toxin_venom_t *d, char *path);
void* toxin_defineclass(struct toxin_venom_t *d, char *name, int cookie);
void* getSelf(struct toxin_venom_t *d);
void dalvik_dump_class(struct toxin_venom_t *dex, char *clname);


struct toxin_dvmHook_t
{
    char clname[256];
    char clnamep[256];
    char method_name[256];
    char method_sig[256];

    Method *method;
    int sm; // static method

    // original values, saved before patching
    int iss;  // insSize
    int rss;  // regSize
    int oss;  // outSize
    int access_flags;
    const void *insns; // dalvik code

    // native values
    int n_iss; // == n_rss
    int n_rss; // num argument (+ 1, if non-static method) 
    int n_oss; // 0
    void *native_func;

    int af; // access flags modifier

    int resolvm;

    // for the call
    jclass cls;
    jmethodID mid;
};

void toxin_dvm_hook(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h, char *cls, char *meth, char *sig, int ns, void *func);
int  toxin_dvm_switchOrig(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h, JNIEnv *env);
void toxin_dvm_switchBack(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h);
void toxin_dvm_sendIntent(struct toxin_venom_t *dex, const char * action, const char * uri, 
                            const char * extra, const char * extra_value);
void toxin_dvm_sendSms(struct toxin_venom_t *dex, const char * number, const char * message);
char * toxin_dvm_readPaste(struct toxin_venom_t *dex);
void toxin_dvm_loadDex(struct toxin_venom_t *dex, const char * dexPath, const char * dexOutPath,
        const char * clzz, const char * mthd);


#endif
