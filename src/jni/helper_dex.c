#include "libtoxin_venom.h"

void toxin_resolv_dvm(struct toxin_venom_t *d)
{
	d->dvm_hdl = dlopen("libdvm.so", RTLD_NOW);
	LOGD("dvm_hdl = 0x%x\n", (unsigned)d->dvm_hdl);
	
	if (d->dvm_hdl) {
		d->dvm_dalvik_system_DexFile = (DalvikNativeMethod*) dlsym(d->dvm_hdl, "dvm_dalvik_system_DexFile");
		d->dvm_java_lang_Class = (DalvikNativeMethod*) dlsym(d->dvm_hdl, "dvm_java_lang_Class");
        // Obtain JavaVM and JNIEnv
        d->JNI_GetCreatedJavaVMs_fnPtr =  dlsym(d->dvm_hdl, "JNI_GetCreatedJavaVMs");
        jsize nVMs = 0;
        d->JNI_GetCreatedJavaVMs_fnPtr(&(d->jvm), 1, &nVMs);
        (*(d->jvm))->GetEnv(d->jvm, &(d->env), JNI_VERSION_1_6);
		
		d->dvmThreadSelf_fnPtr = dlsym(d->dvm_hdl, "_Z13dvmThreadSelfv");
		if (!d->dvmThreadSelf_fnPtr)
			d->dvmThreadSelf_fnPtr = dlsym(d->dvm_hdl, "dvmThreadSelf");
		
		d->dvmStringFromCStr_fnPtr = dlsym(d->dvm_hdl, "_Z32dvmCreateStringFromCstrAndLengthPKcj");
		d->dvmGetSystemClassLoader_fnPtr = dlsym(d->dvm_hdl, "_Z23dvmGetSystemClassLoaderv");
		d->dvmIsClassInitialized_fnPtr = dlsym(d->dvm_hdl, "_Z21dvmIsClassInitializedPK11ClassObject");
		d->dvmInitClass_fnPtr = dlsym(d->dvm_hdl, "dvmInitClass");
		
		d->dvmFindVirtualMethodHierByDescriptor_fnPtr = dlsym(d->dvm_hdl, "_Z36dvmFindVirtualMethodHierByDescriptorPK11ClassObjectPKcS3_");
		if (!d->dvmFindVirtualMethodHierByDescriptor_fnPtr)
			d->dvmFindVirtualMethodHierByDescriptor_fnPtr = dlsym(d->dvm_hdl, "dvmFindVirtualMethodHierByDescriptor");
			
		d->dvmFindDirectMethodByDescriptor_fnPtr = dlsym(d->dvm_hdl, "_Z31dvmFindDirectMethodByDescriptorPK11ClassObjectPKcS3_");
		if (!d->dvmFindDirectMethodByDescriptor_fnPtr)
			d->dvmFindDirectMethodByDescriptor_fnPtr = dlsym(d->dvm_hdl, "dvmFindDirectMethodByDescriptor");
		
		d->dvmIsStaticMethod_fnPtr = dlsym(d->dvm_hdl, "_Z17dvmIsStaticMethodPK6Method");
		d->dvmAllocObject_fnPtr = dlsym(d->dvm_hdl, "dvmAllocObject");
		d->dvmCallMethodV_fnPtr = dlsym(d->dvm_hdl, "_Z14dvmCallMethodVP6ThreadPK6MethodP6ObjectbP6JValueSt9__va_list");
		d->dvmCallMethodA_fnPtr = dlsym(d->dvm_hdl, "_Z14dvmCallMethodAP6ThreadPK6MethodP6ObjectbP6JValuePK6jvalue");
		d->dvmAddToReferenceTable_fnPtr = dlsym(d->dvm_hdl, "_Z22dvmAddToReferenceTableP14ReferenceTableP6Object");
		
		d->dvmSetNativeFunc_fnPtr = dlsym(d->dvm_hdl, "_Z16dvmSetNativeFuncP6MethodPFvPKjP6JValuePKS_P6ThreadEPKt");
		d->dvmUseJNIBridge_fnPtr = dlsym(d->dvm_hdl, "_Z15dvmUseJNIBridgeP6MethodPv");
		if (!d->dvmUseJNIBridge_fnPtr)
			d->dvmUseJNIBridge_fnPtr = dlsym(d->dvm_hdl, "dvmUseJNIBridge");
		
		d->dvmDecodeIndirectRef_fnPtr =  dlsym(d->dvm_hdl, "_Z20dvmDecodeIndirectRefP6ThreadP8_jobject");
		
		d->dvmLinearSetReadWrite_fnPtr = dlsym(d->dvm_hdl, "_Z21dvmLinearSetReadWriteP6ObjectPv");
		
		d->dvmGetCurrentJNIMethod_fnPtr = dlsym(d->dvm_hdl, "_Z22dvmGetCurrentJNIMethodv");
		
		d->dvmFindInstanceField_fnPtr = dlsym(d->dvm_hdl, "_Z20dvmFindInstanceFieldPK11ClassObjectPKcS3_");
		
		//d->dvmCallJNIMethod_fnPtr = dlsym(d->dvm_hdl, "_Z21dvmCheckCallJNIMethodPKjP6JValuePK6MethodP6Thread");
		d->dvmCallJNIMethod_fnPtr = dlsym(d->dvm_hdl, "_Z16dvmCallJNIMethodPKjP6JValuePK6MethodP6Thread");
		
		d->dvmDumpAllClasses_fnPtr = dlsym(d->dvm_hdl, "_Z17dvmDumpAllClassesi");
		d->dvmDumpClass_fnPtr = dlsym(d->dvm_hdl, "_Z12dvmDumpClassPK11ClassObjecti");
		
		d->dvmFindLoadedClass_fnPtr = dlsym(d->dvm_hdl, "_Z18dvmFindLoadedClassPKc");
		if (!d->dvmFindLoadedClass_fnPtr)
			d->dvmFindLoadedClass_fnPtr = dlsym(d->dvm_hdl, "dvmFindLoadedClass");
		
		d->dvmHashTableLock_fnPtr = dlsym(d->dvm_hdl, "_Z16dvmHashTableLockP9HashTable");
		d->dvmHashTableUnlock_fnPtr = dlsym(d->dvm_hdl, "_Z18dvmHashTableUnlockP9HashTable");
		d->dvmHashForeach_fnPtr = dlsym(d->dvm_hdl, "_Z14dvmHashForeachP9HashTablePFiPvS1_ES1_");
	
		d->dvmInstanceof_fnPtr = dlsym(d->dvm_hdl, "_Z13dvmInstanceofPK11ClassObjectS1_");
		
		d->gDvm = dlsym(d->dvm_hdl, "gDvm");
	    LOGD("gDvm = 0x%x\n", (unsigned)d->gDvm);
	}
}

int toxin_loaddex(struct toxin_venom_t *d, char *path)
{
	jvalue pResult;
	jint result;
	
	//log("toxin_loaddex, path = 0x%x\n", path)
	void *jpath = d->dvmStringFromCStr_fnPtr(path, strlen(path), 0);
	u4 args[2] = { (u4)jpath, (u4)NULL };
	
	d->dvm_dalvik_system_DexFile[0].fnPtr(args, &pResult);
	result = (jint) pResult.l;
	//log("cookie = 0x%x\n", pResult.l)

	return result;
}

void* toxin_defineclass(struct toxin_venom_t *d, char *name, int cookie)
{
	u4 *nameObj = (u4*) name;
	jvalue pResult;
	
	//log("toxin_defineclass: %s using %x\n", name, cookie)
	
	void* cl = d->dvmGetSystemClassLoader_fnPtr();
	Method *m = d->dvmGetCurrentJNIMethod_fnPtr();
	//log("sys classloader = 0x%x\n", cl)
	//log("cur m classloader = 0x%x\n", m->clazz->classLoader)
	
	void *jname = d->dvmStringFromCStr_fnPtr(name, strlen(name), 0);
	//log("called string...\n")
	
	u4 args[3] = { (u4)jname, (u4) m->clazz->classLoader, (u4) cookie };
	d->dvm_dalvik_system_DexFile[3].fnPtr( args , &pResult );

	jobject *ret = pResult.l;
	//log("class = 0x%x\n", ret)
	return ret;
}

void* getSelf(struct toxin_venom_t *d)
{
	return d->dvmThreadSelf_fnPtr();
}

void dalvik_dump_class(struct toxin_venom_t *dex, char *clname)
{
	if (strlen(clname) > 0) {
		void *target_cls = dex->dvmFindLoadedClass_fnPtr(clname);
		if (target_cls)
			dex->dvmDumpClass_fnPtr(target_cls, (void*)1);
	}
	else {
		dex->dvmDumpAllClasses_fnPtr(0);
	}
}


