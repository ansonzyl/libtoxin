LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS) 
LOCAL_MODULE    := libtoxin_pin
LOCAL_SRC_FILES := libtoxin_pin.c helper_elf.c helper_ptrace.c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS) 
LOCAL_MODULE    := libtoxin_venom
LOCAL_SRC_FILES := libtoxin_venom.c helper_dex.c
include $(BUILD_STATIC_LIBRARY)
