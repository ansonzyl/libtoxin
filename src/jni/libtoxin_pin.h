#ifndef LIBTOXIN_VITRO_H_
#define LIBTOXIN_VITRO_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <elf.h>
#include <string.h>
#include <errno.h>
#include <android/log.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>

#define  LOGD(fmt, args...)  __android_log_print(ANDROID_LOG_DEBUG,"TOXIN_DEBUG", fmt, ##args)    
#define  LOGE(fmt, args...)  __android_log_print(ANDROID_LOG_ERROR,"TOXIN_ERROR", fmt, ##args)    
#define  LOGI(fmt, args...)  __android_log_print(ANDROID_LOG_INFO,"TOXIN_INFO", fmt, ##args)    

#define pint(_x)  LOGD("[%20s(%04d)] %-30s=0x%08x\n",__FUNCTION__, __LINE__, #_x, (int)(_x))
#define pstr(_x)  LOGD("[%20s(%04d)] %-30s=%s\n",__FUNCTION__,__LINE__, #_x, (char*)(_x))

typedef enum { false=0, true=!false } bool;

/* The definitions of link_map and soinfo are from AOSP */ 
struct link_map {
    uintptr_t l_addr;
    char * l_name;
    uintptr_t l_ld;
    struct link_map * l_next;
    struct link_map * l_prev;
};

#define SOINFO_NAME_LEN 128

struct soinfo {
    char name[SOINFO_NAME_LEN];
    const Elf32_Phdr* phdr;
    int phnum;
    unsigned entry;
    unsigned base;
    unsigned size;
    int unused;         // DO NOT USE, maintained for compatibility.
    unsigned* dynamic;
    unsigned unused2;   // DO NOT USE, maintained for compatibility
    unsigned unused3;   // DO NOT USE, maintained for compatibility
    struct soinfo* next;
    unsigned flags;
    const char* strtab;
    Elf32_Sym* symtab;
    unsigned nbucket;
    unsigned nchain;
    unsigned* bucket;
    unsigned* chain;
    unsigned* plt_got;
    Elf32_Rel* plt_rel;
    unsigned plt_rel_count;
    Elf32_Rel* rel;
    unsigned rel_count;
    unsigned* preinit_array;
    unsigned preinit_array_count;
    unsigned* init_array;
    unsigned init_array_count;
    unsigned* fini_array;
    unsigned fini_array_count;
    void (*init_func)();
    void (*fini_func)();
    unsigned refcount;
    struct link_map linkmap;
};


typedef struct pt_regs regs_t;

/*dl function list */
struct dl_fl{
    long l_dlopen;
    long l_dlclose;
    long l_dlsym;
};

struct dyn_info{
    Elf32_Addr symtab;
    Elf32_Addr strtab;
    Elf32_Addr jmprel;
    Elf32_Word totalrelsize;
    Elf32_Word relsize;
    Elf32_Word nrels;
};

struct elf_info {
    int pid;
    Elf32_Addr base;
    Elf32_Ehdr ehdr;
    Elf32_Phdr phdr;
    Elf32_Dyn dyn;
    Elf32_Addr dynaddr;
    Elf32_Word got;
    Elf32_Addr phdr_addr;
    Elf32_Addr map_addr;
    Elf32_Word nchains;
};

#define IS_DYN(_einfo) (_einfo->ehdr.e_type == ET_DYN)

typedef enum {
    PAT_INT,
    PAT_STR,
    PAT_MEM
}toxin_arg_type;

typedef struct {
    toxin_arg_type type;
    unsigned long _stackid; //private only visible in toxin_call
    union {
        int i;
        char *s;
        struct {
            int size;
            void *addr;
        }mem;
    };
}toxin_arg;

typedef struct dl_fl dl_fl_t;

/* ptrace related helpers */
void toxin_attach(pid_t pid);
void toxin_cont(pid_t pid);
void toxin_detach(pid_t pid);
void toxin_write(pid_t pid, unsigned long addr, void *vptr, int len);
void toxin_read(pid_t pid, unsigned long addr, void *vptr, int len);
char * toxin_readstr(pid_t pid, unsigned long addr);
void toxin_readreg(pid_t pid, regs_t *regs);
void toxin_writereg(pid_t pid, regs_t *regs);
unsigned long toxin_push(pid_t pid, regs_t *regs, void *paddr, int size);
long toxin_stack_alloc(pid_t pid, regs_t *regs, int size);
dl_fl_t * toxin_find_dlinfo(int pid);
void * toxin_dlopen(pid_t pid, const char *filename, int flag);
void * toxin_dlsym(pid_t pid, void *handle, const char *symbol) ;
int toxin_dlclose(pid_t pid, void *handle) ;
int toxin_wait_for_signal (int pid, int signal) ;

/* elf related helpers */
void set_hdr_addr(int imgaddr,int offaddr);
void get_elf_info(int pid, Elf32_Addr base, struct elf_info *einfo);
void get_sym_info(int pid, struct link_map *lm) ;
unsigned long find_symbol(int pid, struct link_map *map, char *sym_name) ;
unsigned long find_symbol_in_linkmap(int pid, struct link_map *lm,char *sym_name);
unsigned long find_sym_in_rel(struct elf_info *einfo, const char *sym_name);
void get_dyn_info(struct elf_info *einfo, struct dyn_info *dinfo);

void toxin_replace_native_func(int pid, const char *orig_func, const char *inj_lib, const char *inj_func);
int  toxin_inject_and_call(int pid, const char *inj_lib, const char *inj_func, int argc, toxin_arg *argv, bool clean);

#endif
