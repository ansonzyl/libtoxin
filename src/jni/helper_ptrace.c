#include "libtoxin_pin.h"

static regs_t oldregs;
dl_fl_t ldl;

void toxin_attach(int pid) {
    regs_t regs;
    errno = 0;
    if (ptrace(PTRACE_ATTACH, pid, NULL, NULL ) < 0) {
        LOGE("toxin attach failed: %d", errno);
        exit(-1);
    }
    if(toxin_wait_for_signal(pid, SIGSTOP) != 1){
        LOGE("toxin attach Stage 1 failed");
        exit(-1);
    }

    toxin_readreg(pid, &regs);
    memcpy(&oldregs, &regs, sizeof(regs));

    regs.ARM_pc = 0x11;
    regs.ARM_cpsr |=0x30;

    toxin_writereg(pid, &regs);

    toxin_cont(pid);

    if(toxin_wait_for_signal(pid, SIGSEGV) != 1){
        LOGE("toxin attach Stage 2 failed");
        exit(-1);
    }
    LOGI("toxin attach ok!");

}

void toxin_cont(int pid) {
    if (ptrace(PTRACE_CONT, pid, NULL, NULL ) < 0) {
        LOGE("toxin cont failed");
        exit(-1);
    }
}

void toxin_detach(int pid) {
    toxin_writereg(pid, &oldregs);
    if (ptrace(PTRACE_DETACH, pid, NULL, NULL ) < 0) {
        LOGE("toxin detach failed");
        exit(-1);
    }
}

void toxin_write(int pid, unsigned long addr, void *vptr, int len) {
    int count;
    long word;
    void *src = (long*) vptr;
    count = 0;

    while (count < len) {
        errno = 0;
        memcpy(&word, src + count, sizeof(word));
        word = ptrace(PTRACE_POKETEXT, pid, (void*) (addr + count), (void*) word);
        count += 4;

        if (errno != 0)
            LOGE("toxin write failed\t %ld\n", addr + count);
    }
}

void toxin_read(int pid, unsigned long addr, void *vptr, int len) {
    int i, count;
    long word;
    unsigned long *ptr = (unsigned long *) vptr;

    i = count = 0;

    while (count < len) {
        word = ptrace(PTRACE_PEEKTEXT, pid, (void*) (addr + count), NULL );
        count += 4;
        ptr[i++] = word;
    }
}

char * toxin_readstr(int pid, unsigned long addr) {
    char *str = (char *) malloc(64);
    int i, count;
    long word;
    char *pa;

    i = count = 0;
    pa = (char *) &word;

    while (i <= 60) {
        word = ptrace(PTRACE_PEEKTEXT, pid, (void*) (addr + count), NULL );
        count += 4;

        if (pa[0] == '\0') {
            str[i] = '\0';
            break;
        } else
            str[i++] = pa[0];

        if (pa[1] == '\0') {
            str[i] = '\0';
            break;
        } else
            str[i++] = pa[1];

        if (pa[2] == '\0') {
            str[i] = '\0';
            break;
        } else
            str[i++] = pa[2];

        if (pa[3] == '\0') {
            str[i] = '\0';
            break;
        } else
            str[i++] = pa[3];
    }
    return str;
}

void toxin_readreg(int pid, regs_t *regs) {
    if (ptrace(PTRACE_GETREGS, pid, NULL, regs))
        LOGE("*** toxin_readreg error ***\n");

}

void toxin_writereg(int pid, regs_t *regs) {
    if (ptrace(PTRACE_SETREGS, pid, NULL, regs))
        LOGE("*** toxin_writereg error ***\n");
}

unsigned long toxin_push(int pid, regs_t *regs, void *paddr, int size) {
    unsigned long arm_sp;
    arm_sp = regs->ARM_sp;
    arm_sp -= size;
    arm_sp = arm_sp - arm_sp % 4;
    regs->ARM_sp= arm_sp;
    toxin_write(pid, arm_sp, paddr, size);
    return arm_sp;
}

long toxin_stack_alloc(pid_t pid, regs_t *regs, int size) {
    unsigned long arm_sp;
    arm_sp = regs->ARM_sp;
    arm_sp -= size;
    arm_sp = arm_sp - arm_sp % 4;
    regs->ARM_sp= arm_sp;
    return arm_sp;
}

void * toxin_dlopen(pid_t pid, const char *filename, int flag) {
    regs_t regs;
    toxin_readreg(pid, &regs);

    regs.ARM_lr = 1;

    regs.ARM_r0= (long)toxin_push(pid,&regs, (void*)filename,strlen(filename)+1);
    regs.ARM_r1= flag;
    regs.ARM_pc= ldl.l_dlopen;
    toxin_writereg(pid, &regs);
    toxin_cont(pid);
    if(toxin_wait_for_signal(pid, SIGSEGV) != 1){
        LOGE("toxin dlopen failed!");
        exit(-1);
    }
    LOGI("toxin dlopen ok!");
    toxin_readreg(pid, &regs);
    return (void*) regs.ARM_r0;
}

int toxin_dlclose(pid_t pid, void * handle) {
    regs_t regs;
    toxin_readreg(pid, &regs);

    regs.ARM_lr = 1;

    regs.ARM_r0= (long)handle;
    regs.ARM_pc= ldl.l_dlclose;
    toxin_writereg(pid, &regs);
    toxin_cont(pid);
    if(toxin_wait_for_signal(pid, SIGSEGV) != 1){
        LOGE("toxin dlclose failed!");
        exit(-1);
    }
    LOGI("toxin dlclose ok!");
    toxin_readreg(pid, &regs);
    return regs.ARM_r0;
}

void * toxin_dlsym(pid_t pid, void *handle, const char *symbol) {
    regs_t regs;
    toxin_readreg(pid, &regs);

    regs.ARM_lr = 1;

    regs.ARM_r0= (long)handle;
    regs.ARM_r1= (long)toxin_push(pid,&regs, (void*)symbol,strlen(symbol)+1);

    regs.ARM_pc= ldl.l_dlsym;
    toxin_writereg(pid, &regs);
    toxin_cont(pid);
    if(toxin_wait_for_signal(pid, SIGSEGV) != 1){
        LOGE("toxin dlsym failed!");
        exit(-1);
    }
    LOGI("toxin dlsym ok!");
    toxin_readreg(pid, &regs);
    return (void*) regs.ARM_r0;
}

int toxin_wait_for_signal(int pid, int signal) {
    int status;
    pid_t res;
    res = waitpid(pid, &status, 0);
    if (res != pid || !WIFSTOPPED (status))
        return status;
    return WSTOPSIG (status) == signal;
}

static Elf32_Addr get_linker_base(int pid, Elf32_Addr *base_start, Elf32_Addr *base_end) {
    unsigned long base = 0;
    char mapname[FILENAME_MAX];
    memset(mapname, 0, FILENAME_MAX);
    snprintf(mapname, FILENAME_MAX, "/proc/%d/maps", pid);
    FILE *file = fopen(mapname, "r");
    *base_start = *base_end = 0;
    if (file) {
        while (1) {
            unsigned int atleast = 32;
            int xpos = 20;
            char startbuf[9];
            char endbuf[9];
            char line[FILENAME_MAX];
            memset(line, 0, FILENAME_MAX);
            char *linestr = fgets(line, FILENAME_MAX, file);
            if (!linestr) {
                break;
            }
            if (strlen(line) > atleast && strstr(line, "/system/bin/linker")) {
                memset(startbuf, 0, sizeof(startbuf));
                memset(endbuf, 0, sizeof(endbuf));

                memcpy(startbuf, line, 8);
                memcpy(endbuf, &line[8 + 1], 8);
                if (*base_start == 0) {
                    *base_start = strtoul(startbuf, NULL, 16);
                    *base_end = strtoul(endbuf, NULL, 16);
                    base = *base_start;
                } else {
                    *base_end = strtoul(endbuf, NULL, 16);
                }
            }
        }
        fclose(file);
    }
    return base;
}

#define LIBDLSO "libdl.so"
dl_fl_t * toxin_find_dlinfo(int pid) {
    Elf32_Sym sym;
    Elf32_Addr addr;
    struct soinfo lsi;
    Elf32_Addr base_start = 0;
    Elf32_Addr base_end = 0;
    Elf32_Addr base = get_linker_base(pid, &base_start, &base_end);

    if (base == 0) {
        LOGD("no linker found\n");
        return NULL ;
    } else {
        LOGD("search libdl.so from 0x%08x to 0x%08x\n", base_start, base_end);
    }

    for (addr = base_start; addr < base_end; addr += 4) {
        char soname[strlen(LIBDLSO)];
        Elf32_Addr off = 0;

        toxin_read(pid, addr, soname, strlen(LIBDLSO));
        if (strncmp(LIBDLSO, soname, strlen(LIBDLSO))) {
            continue;
        }

        LOGD("soinfo found at 0x%08x\n", addr);
        toxin_read(pid, addr, &lsi, sizeof(lsi));

        off = (Elf32_Addr)lsi.symtab;

        toxin_read(pid, off, &sym, sizeof(sym));
        off += sizeof(sym);

        toxin_read(pid, off, &sym, sizeof(sym));
        ldl.l_dlopen = sym.st_value;
        off += sizeof(sym);

        toxin_read(pid, off, &sym, sizeof(sym));
        ldl.l_dlclose = sym.st_value;
        off += sizeof(sym);

        toxin_read(pid, off, &sym, sizeof(sym));
        ldl.l_dlsym = sym.st_value;
        off += sizeof(sym);

        LOGD("dlopen addr %p\n", (void*) ldl.l_dlopen);
        LOGD("dlclose addr %p\n", (void*) ldl.l_dlclose);
        LOGD("dlsym addr %p\n", (void*) ldl.l_dlsym);
        return &ldl;
    }
    LOGD("%s not found!\n", LIBDLSO);
    return NULL ;
}

