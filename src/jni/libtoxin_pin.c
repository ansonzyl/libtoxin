#include <stdio.h>
#include "libtoxin_pin.h"

void toxin_replace_native_func(int pid, const char *orig_func, \
        const char *inj_lib, const char *inj_func) {
    void *handle = NULL;
    FILE *m = NULL;
    char maps[80], line[200], soaddrs[20], soaddr[10], soname[60], prop[10];
    unsigned hook_func, base;

    toxin_attach(pid);
    toxin_find_dlinfo(pid);
    handle = toxin_dlopen(pid, inj_lib, 1); //RTLD_LAZY
    LOGD("poisson lib injected: %p\n", handle);
    hook_func = (unsigned) toxin_dlsym(pid, handle, inj_func);
    LOGD("poisson func located: %08x\n", hook_func);


    memset(maps,0,sizeof(maps));
    memset(soaddrs,0,sizeof(soaddrs));
    memset(soaddr,0,sizeof(soaddr));
    sprintf(maps,"/proc/%d/maps",pid);
    m = fopen(maps,"r");
    if(!m)
    {
        LOGE("open %s error!\n",maps);
    }
    while(fgets(line,sizeof(line),m))
    {
        int in_so_list = 0;
        struct elf_info einfo;
        unsigned tmpaddr = 0;

        if(strstr(line, "r-xp") == NULL)
            continue;
        if(strstr(line, inj_lib) != NULL)
            continue;

        sscanf(line,"%s %s %*s %*s %*s %s",soaddrs,prop,soname);
        sscanf(soaddrs,"%[^-]",soaddr);
        LOGD("#### %s %s %s\n",soaddr,prop,soname);
        base = strtoul(soaddr, NULL, 16);
        get_elf_info(pid, base, &einfo);
        tmpaddr = find_sym_in_rel(&einfo, orig_func);

        if(tmpaddr != 0) {
            LOGI("toxin found a target!");
            toxin_write(pid,tmpaddr,&hook_func,4);
        }
    }

    toxin_detach(pid);
}

int toxin_inject_and_call(int pid, const char *inj_lib, const char *inj_func, 
        int argc, toxin_arg *argv, bool clean) {
    toxin_attach(pid);
    toxin_find_dlinfo(pid);
    void * handle = toxin_dlopen(pid, inj_lib, 2); //RTLD_NOW
    LOGD("poisson lib injected: %p", handle);
    unsigned hook_func = (unsigned) toxin_dlsym(pid, handle, inj_func);
    LOGD("poisson func located: %08x", hook_func);

    int i = 0;
    regs_t regs;
    toxin_readreg(pid, &regs);

    /*prepare stacks*/
    for (i = 0; i < argc; i++) {
        toxin_arg *arg = &argv[i];
        if (arg->type == PAT_STR) {
            arg->_stackid = toxin_push(pid, &regs, arg->s, strlen(arg->s) + 1);
        } else if (arg->type == PAT_MEM) {
            arg->_stackid = toxin_push(pid, &regs, arg->mem.addr, arg->mem.size);
        }
    }
    for (i = 0; (i < 4) && (i < argc); i++) {
        toxin_arg *arg = &argv[i];
        if (arg->type == PAT_INT) {
            regs.uregs[i] = arg->i;
        } else if (arg->type == PAT_STR) {
            regs.uregs[i] = arg->_stackid;
        } else if (arg->type == PAT_MEM) {
            regs.uregs[i] = arg->_stackid;
        } else {
            LOGD("unkonwn arg type\n");
        }
    }

    for (i = argc - 1; i >= 4; i--) {
        toxin_arg *arg = &argv[i];
        if (arg->type == PAT_INT) {
            toxin_push(pid, &regs, &arg->i, sizeof(int));
        } else if (arg->type == PAT_STR) {
            toxin_push(pid, &regs, &arg->_stackid, sizeof(unsigned long));
        } else if (arg->type == PAT_MEM) {
            toxin_push(pid, &regs, &arg->_stackid, sizeof(unsigned long));
        } else {
            LOGD("unkonwn arg type\n");
        }
    }
    regs.ARM_lr = 1;
    regs.ARM_pc= hook_func;
    toxin_writereg(pid, &regs);
    toxin_cont(pid);
    if(toxin_wait_for_signal(pid, SIGSEGV) != 1){
        LOGE("toxin call failed!");
        exit(-1);
    }
    LOGI("toxin call ok!");
    toxin_readreg(pid, &regs);

    //sync memory
    for (i = 0; i < argc; i++) {
        toxin_arg *arg = &argv[i];
        if (arg->type == PAT_STR) {
        } else if (arg->type == PAT_MEM) {
            toxin_read(pid, arg->_stackid, arg->mem.addr, arg->mem.size);
        }
    }

    if(clean)
        toxin_dlclose(pid, handle);
    toxin_detach(pid);
    return regs.ARM_r0;
}
