#include "libtoxin_pin.h"

void get_elf_info(int pid, Elf32_Addr base, struct elf_info *einfo) {
    int i = 0;
    einfo->pid = pid;

    /* Read ELF Header */
    einfo->base = base;
    toxin_read(pid, einfo->base, &einfo->ehdr, sizeof(Elf32_Ehdr));
    einfo->phdr_addr = einfo->base + einfo->ehdr.e_phoff;

    /* Read Program Header */
    toxin_read(pid, einfo->phdr_addr, &einfo->phdr, sizeof(Elf32_Phdr));

    /* Search the DYNAMIC segment */
    while (einfo->phdr.p_type != PT_DYNAMIC) {
        toxin_read(pid, einfo->phdr_addr += sizeof(Elf32_Phdr), &einfo->phdr, sizeof(Elf32_Phdr));
    }
    einfo->dynaddr =  (IS_DYN(einfo)?einfo->base:0) + einfo->phdr.p_vaddr;

    /* Seach the GOT table */
    toxin_read(pid, einfo->dynaddr, &einfo->dyn, sizeof(Elf32_Dyn));
    while (einfo->dyn.d_tag != DT_PLTGOT) {
        toxin_read(pid, einfo->dynaddr + i * sizeof(Elf32_Dyn), &einfo->dyn, sizeof(Elf32_Dyn));
        i++;
    }

    einfo->got = (IS_DYN(einfo)?einfo->base:0) + (Elf32_Word) einfo->dyn.d_un.d_ptr;
    toxin_read(pid, einfo->got+4, &einfo->map_addr, 4);
}

unsigned long find_sym_in_rel(struct elf_info *einfo, const char *sym_name) {
    Elf32_Rel rel;
    Elf32_Sym sym;
    unsigned int i;
    char *str = NULL;
    unsigned long ret;
    struct dyn_info dinfo;

    get_dyn_info(einfo, &dinfo);
    for (i = 0; i < dinfo.nrels; i++) {
        toxin_read(einfo->pid, (unsigned long) (dinfo.jmprel + i * sizeof(Elf32_Rel)), &rel, sizeof(Elf32_Rel));

        if (ELF32_R_SYM(rel.r_info)) {
            toxin_read(einfo->pid, dinfo.symtab + ELF32_R_SYM(rel.r_info) * sizeof(Elf32_Sym), &sym, sizeof(Elf32_Sym));
            str = toxin_readstr(einfo->pid, dinfo.strtab + sym.st_name);
            if (strcmp(str, sym_name) == 0) {
                free(str);
                break;
            }
            free(str);
        }
    }

    if (i == dinfo.nrels)
        ret = 0;
    else
        ret = (IS_DYN(einfo)?einfo->base:0) + rel.r_offset;

    return ret;
}

void get_dyn_info(struct elf_info *einfo, struct dyn_info *dinfo) {
    Elf32_Dyn dyn;
    int i = 0;

    toxin_read(einfo->pid, einfo->dynaddr + i * sizeof(Elf32_Dyn), &dyn, sizeof(Elf32_Dyn));
    i++;
    while (dyn.d_tag) {
        switch (dyn.d_tag) {
            case DT_SYMTAB:
                dinfo->symtab = (IS_DYN(einfo)?einfo->base:0) + dyn.d_un.d_ptr;
                break;
            case DT_STRTAB:
                dinfo->strtab = (IS_DYN(einfo)?einfo->base:0) + dyn.d_un.d_ptr;
                break;
            case DT_JMPREL:
                dinfo->jmprel = (IS_DYN(einfo)?einfo->base:0) + dyn.d_un.d_ptr;
                break;
            case DT_PLTRELSZ:
                dinfo->totalrelsize = dyn.d_un.d_val;
                break;
            case DT_RELAENT:
                dinfo->relsize = dyn.d_un.d_val;
                break;
            case DT_RELENT:
                dinfo->relsize = dyn.d_un.d_val;
                break;
        }
        toxin_read(einfo->pid, einfo->dynaddr + i * sizeof(Elf32_Dyn), &dyn, sizeof(Elf32_Dyn));
        i++;
    }
    if (dinfo->relsize == 0) {
        dinfo->relsize = 8;
    }
    dinfo->nrels = dinfo->totalrelsize / dinfo->relsize;
}
