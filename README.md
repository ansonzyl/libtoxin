# Toxin to poisson the Droid

## Features

* Inject library into victim memory space
* Hook victim native functions to functions defined in the injected library
* Directly invoke some methods defined in the injected library (w/o hooking)
* Hook victim dalvik functions to jni functions defined in the injected library

## Prerequisites

Android SDK and NDK (NDK is mandatory and **ndk-build** should be in the path)

## Description

* libtoxin\_pin: helper lib for attack process to "pin" into the victim process
* libtoxin\_venom: helper lib for the payload injected into the victim process

* example1: inject and hook native function
* example2: inject and call native function + hook dalvik function **String.startsWith()**

## How to Build

* make (build toxin libraries)
* make example1/example2/... (build examples)
* make install (initialze on experiment device)
* make uninstall (clean on experiment device)

