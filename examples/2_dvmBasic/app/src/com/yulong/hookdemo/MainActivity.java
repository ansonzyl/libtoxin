package com.yulong.hookdemo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	private void copyAssets(String filename) {
		AssetManager assetManager = getAssets();
		InputStream in = null;
		OutputStream out = null;
		byte[] buffer = new byte[1024];
		int read;
		
		try {
			in = assetManager.open(filename);
			File outFile = new File(getFilesDir().getPath(), filename);
			out = new FileOutputStream(outFile);

			while ((read = in.read(buffer)) != -1) 
				out.write(buffer, 0, read);
						
			in.close();
			out.flush();
			out.close();
		} catch (IOException e) {
			Log.e("tag", "Failed to copy asset file: " + filename, e);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TextView text = (TextView) findViewById(R.id.txt);
		
		text.setText("Does 'hello_world' begin with 'foo'? ");
		text.append(Boolean.toString("hello_world".startsWith("foo")));
		
		text.append("\n\nNow let's hack String.startsWith()\n\n");
		int pid = android.os.Process.myPid();
		copyAssets("test");
		copyAssets("libtoxin_inj.so");
		try {
			Runtime.getRuntime().exec("chmod 777 " + getFilesDir().getPath() + "/test");
			Runtime.getRuntime().exec("chmod 777 " + getFilesDir().getPath() + "/libtoxin_inj.so");
			Runtime.getRuntime().exec(getFilesDir().getPath() + "/test " + pid + " " + getFilesDir().getPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		SystemClock.sleep(2000);
		text.append("Does 'hello_world' begin with 'foo'? ");
		text.append(Boolean.toString("hello_world".startsWith("foo")));
		Log.e("TOXIN_USER", Boolean.toString("hello_world".startsWith("foo")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
