#include <stdio.h>
#include "libtoxin_pin.h"

/*static int myGetppid(int pid)
{
    int ppid = -1;
    char proc_path[100], tmp[10];
    strcat(proc_path, "/proc/");
    sprintf(tmp, "%d", pid);
    strcat(proc_path, tmp);
    strcat(proc_path, "/stat");

    FILE *fp = fopen(proc_path, "r");
    fscanf(fp, "%s %s %s %d %s", tmp, tmp, tmp, &ppid, tmp);
    fclose(fp);

    return ppid;
}*/

int main(int argc, char * argv[])
{
    LOGE("Hello, PoC entered!");
    char path[100], proc_path[100], packName[100], tmp[50];
    int ppid = getppid(); // pid of sh
    //int gppid = myGetppid(ppid); // pid of app

    LOGE("ppid: %d", ppid);
    sprintf(tmp, "%d", ppid);
    strcpy(proc_path, "/proc/");
    strcat(proc_path, tmp);
    strcat(proc_path, "/cmdline");

    LOGE("proc_path: %s", proc_path);

    FILE *fp = fopen(proc_path, "r");
    fscanf(fp, "%s", packName);
    fclose(fp);
    
    strcpy(path, "/data/data/");
    strcat(path, packName);
    strcat(path, "/libtoxin_inj.so");

    LOGE("path: %s", path);

    toxin_inject_and_call(ppid, path, "my_func",0,NULL,0);
    return 0;
}
