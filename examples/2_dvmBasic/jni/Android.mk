LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS) 
LOCAL_MODULE := libtoxin_pin
LOCAL_SRC_FILES := ../../../out/libtoxin_pin.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES  := ../../src/jni/
LOCAL_MODULE    := test
LOCAL_SRC_FILES := test.c 
LOCAL_STATIC_LIBRARIES := libtoxin_pin
LOCAL_LDLIBS    := -llog
include $(BUILD_EXECUTABLE)


# payload to be injected and to poisson dvm

include $(CLEAR_VARS) 
LOCAL_MODULE := libtoxin_venom
LOCAL_SRC_FILES := ../../../out/libtoxin_venom.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES  := ../../src/jni/
LOCAL_SRC_FILES:=  toxin_inj.c
LOCAL_MODULE := libtoxin_inj
LOCAL_STATIC_LIBRARIES := libtoxin_venom
LOCAL_LDLIBS    := -llog
include $(BUILD_SHARED_LIBRARY)
