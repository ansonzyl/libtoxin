#include <stdio.h>

int my_func(const char *str) {
    printf("*(^_^)* A hooked puts() is called\n");
    return puts(str);
}
