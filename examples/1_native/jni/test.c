#include <stdio.h>
#include "libtoxin_pin.h"

int main(int argc, char * argv[])
{
    if(argc != 2){
        printf("Tell me the target pid please.\n");
        return -1;
    }

    int pid = atoi(argv[1]);
    toxin_replace_native_func(pid, "puts", "/data/libtoxin_inj.so", "my_func");
}
